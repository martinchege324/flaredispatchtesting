﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
namespace FlareDispatchAutomations
{
    class Program
    {
        static void Main(string[] args)
        {
      
        }

        //initialize web driver and web URL
        [SetUp]
        public void Initialize()
        {
            FlareProperties.driver = new ChromeDriver();
            FlareProperties.driver.Navigate().GoToUrl("https://staging.flaredispatch.com/");
        }

        //test whether the user is able to log in the system
        [Test]
        public void PageLogin()
        {
            LoginPageObject LoginPage = new LoginPageObject();
            DispatchPageObject dispatch = LoginPage.Login("martin@flare.co.ke","Flare123");
        }
        //tests forgot password section
        [Test]
        public void PageForgot()
        {
            LoginPageObject Forgot = new LoginPageObject();
            Forgot.ForgotPassword();
        }
        //close the browser
        [TearDown]
        public void WrapUP()
        {
            FlareProperties.driver.Close();
        }
    }
}
