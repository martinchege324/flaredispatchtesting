﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace FlareDispatchAutomations
{
    //Login Page Objects
    class LoginPageObject
    {
        //Constructor
        public LoginPageObject()
        {
            PageFactory.InitElements(FlareProperties.driver, this);
        }
        //Identification of Elements on the page
        [FindsBy(How = How.Name, Using = "email")]
        public IWebElement Email { get; set; }
        [FindsBy(How = How.Name, Using = "password")]
        public IWebElement Password { get; set; }
        [FindsBy(How = How.ClassName, Using = "login__button")]
        public IWebElement LoginBtn { get; set; }
        [FindsBy(How = How.LinkText, Using = "Forgot Password?")]
        public IWebElement ForgotPwd { get; set; }
        [FindsBy(How = How.LinkText, Using = "Back to Login")]
        public IWebElement BackToLogin { get; set; }

        //Login Form
        public DispatchPageObject Login(string UserEmail, string UserPassword)
        {
            SetMethods.EnterText(Email, UserEmail);
            SetMethods.EnterText(Password, UserPassword);
            SetMethods.Click(LoginBtn);
            return new DispatchPageObject();
        }
        //Forgot Password
        public void ForgotPassword()
        {
            SetMethods.Click(ForgotPwd);
            SetMethods.WaitUntilElementIsPresent(BackToLogin);
            SetMethods.Click(BackToLogin);
        }

    }
}
