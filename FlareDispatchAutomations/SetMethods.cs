﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;

namespace FlareDispatchAutomations
{
    class SetMethods
    {
        //Enter Text
        public static void EnterText(IWebElement element, string value)
        {
            element.SendKeys(value);
        }
        //Click Operation
        public static void Click(IWebElement element)
        {
            element.Click();
        }
        //Wait Function
        public static bool WaitUntilElementIsPresent(IWebElement element, int timeout = 10)
        {
            for (var i = 0; i < timeout; i++)
            {
                if (element != null) return true;
                Thread.Sleep(1000);
            }
            return false;
        }
    }
}
